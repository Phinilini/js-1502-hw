// 1. Переписать if
let a, b, result;

a + b < 4 ? result = 'Мало' : result = 'Много';

// 2. Переписать if..else
let message, login = "Вася";

login == 'Вася' ? message = 'Привет' :
login == 'Директор' ? message = 'Здравствуйте' :
login == '' ? message = 'Нет логина' :
message = '';

// Задача 1.1
let A = 1, B = 15, summ = 0;

for (let i = A + 1; i < B; i++) {
	summ += i;
}

document.write("A: " + A + " B: " + B);
document.write("<br/>");
document.write("Сумма: " + summ);
document.write("<br/>");

document.write("<br/>");

// Задача 1.2

document.write("Все нечётные значения: ");
for (let i = A + 1; i < B; i++) {
	if (i % 2 === 1)
		document.write(i + ", ");
}
document.write("<br/>");

document.write("<br/>");

// Задача 2

// Прямоугольник
for (let i = 0; i < 5; i++) {
	for (let j = 0; j < 20; j++) {
		document.write("$");
	}
	document.write("<br/>");
}

document.write("<br/>");

// Прямоугольный треугольник
for (let i = 0; i < 10; i++) {
	for (let j = 0; j < i + 1; j++) {
		document.write("$");
	}
	document.write("<br/>");
}

document.write("<br/>");
document.write("<br/>");

// Равносторонний треугольник h = 20; a = 23
for (let i = 1, j = 11, k = 1; i < 20; i++) {
	if (i % 2 == 1 && i != 1) {
		j--;
		k += 6;
	}
	for (let jtemp = j*5.5; jtemp > 0; jtemp--) {
		document.write("&nbsp;");
	}
	for (let ktemp = k; ktemp > 0; ktemp--) {
		document.write("$");
	}
	document.write("<br/>");
}

document.write("<br/>");
document.write("<br/>");

// Ромб

let i, j, k;
for (i = 1, j = 11, k = 1; i < 20; i++) {
	if (i % 2 == 1 && i != 1) {
		j--;
		k += 6;
	}
	for (let jtemp = j*5.5; jtemp > 0; jtemp--) {
		document.write("&nbsp;");
	}
	for (let ktemp = k; ktemp > 0; ktemp--) {
		document.write("$");
	}
	document.write("<br/>");
}
while (i > 1) {
	if (i % 2 == 1 && i != 1) {
		j++;
		k -= 6;
	}
	for (let jtemp = j*5.5; jtemp > 0; jtemp--) {
		document.write("&nbsp;");
	}
	for (let ktemp = k; ktemp > 0; ktemp--) {
		document.write("$");
	}
	document.write("<br/>");
	i--;
}